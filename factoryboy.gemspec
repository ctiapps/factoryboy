$LOAD_PATH << File.expand_path("../lib", __FILE__)
require 'factoryboy/version'

Gem::Specification.new do |s|
  s.name        = %q{factoryboy}
  s.version     = Factoryboy::VERSION
  s.summary     = %q{factoryboy is factory_girl inspired framework and DSL for defining and
                      using model instance factories.}
  s.description = %q{THIS IS A TEST GEM, DON'T USE IT IN PRODUCTION!
                      factoryboy is factory_girl inspired framework and DSL for defining and
                      using factories - less error-prone, more explicit, and
                      all-around easier to work with than fixtures.}

  s.files         = `git ls-files`.split("\n").reject { |f| f.match(%r{^(spec|features)/}) }

  s.require_path = 'lib'
  s.required_ruby_version = Gem::Requirement.new(">= 1.9.2")

  s.authors = ["Andrius Kairiukstis", "Bartek Dymowski"]
  s.email   = ["k@andrius.mobi", "bartek.dymowski@tooploox.com"]

  s.homepage = "https://github.com/thoughtbot/factory_girl"

  s.add_dependency("activesupport", ">= 3.0.0")


  s.add_development_dependency "bundler", "~> 1.13"
  s.add_development_dependency "rake", "~> 10.0"
  s.add_development_dependency "rspec", "~> 3.0"
  s.add_development_dependency("rspec-its", "~> 1.0")
  # s.add_development_dependency("cucumber", "~> 1.3.15")
  # s.add_development_dependency("timecop")

  s.license = "MIT"
end

