module Factoryboy
  class Builder
    attr_reader :members, :build_class

    def build(options = {})
      @members.unshift(build_class.new(options))
    end

    def initialize(class_name)
      @members = []
      @build_class = class_name
    end
  end
end
