require "factoryboy/version"
require "factoryboy/factoryboy.rb"

module Factoryboy
  FactoryboyError = Class.new(StandardError)

  class << self
  def define_factory(class_name)
    if class_name.is_a? Class
      build_class = class_name
      Factoryboy::Builder.new(build_class)
    else
      raise FactoryboyError.new "#{class_name} is not a class!"
    end
  end
  end
end
