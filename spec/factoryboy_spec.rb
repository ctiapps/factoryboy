require "spec_helper"

class TestClass
end

describe Factoryboy do
  it "has a version number" do
    expect(Factoryboy::VERSION).not_to be nil
  end

  it "should allow to define a factory for defined class" do
    expect(Factoryboy.define_factory(TestClass)).to be_truthy
  end

  it "should not create instance for unknown factory" do
    Unknown = 42
    expect { Factoryboy.define_factory(Unknown) }.to raise_error
  end

end
